package sprite

import (
	"fmt"
	"golang.org/x/net/html"
	"html/template"
	"strings"
)

// An Icon represents any SVG icon Sprite can handle
type Icon interface {
	XML() string
}

// Map represents an SVG sprite-map
type Map struct {
	URL   string
	Icons map[string]Icon
	xml   string
}

// Add adds an Icon to the Map
func (m *Map) Add(name string, icon Icon) {
	if m.Icons == nil {
		m.Icons = make(map[string]Icon, 1)
	}
	m.Icons[name] = icon
}

// AddAll adds all icons in a map to the Map
func (m *Map) AddAll(icons map[string]Icon) {
	for name, icon := range icons {
		m.Add(name, icon)
	}
}

// Parse attempts to parse the sprite-map's icons
func (m *Map) Parse() error {
	xml, err := parse(m.Icons)
	if err != nil {
		return err
	}
	m.xml = xml
	return nil
}

// XML returns parse xml if present, otherwise a text SVG with "No Icons"
func (m *Map) XML() string {
	if m.xml != "" {
		return m.xml
	}
	return `<svg xmlns="http://www.w3.org/2000/svg"><text x="24" y="24">No Icons</text></svg>`
}

// XLink returns a usable XLink for this Map
func (m *Map) XLink(name string) *XLink {
	return &XLink{
		URL:  m.URL,
		Name: name,
	}
}

// New returns a new Map
func New() *Map {
	return &Map{Icons: make(map[string]Icon, 0)}
}

// XLink represents an XLink object for easy xlink generation
type XLink struct {
	URL   string
	Name  string
	Class string
	Id    string
	Style string
}

// XML returns the formatted xlink
func (x XLink) XML() string {
	return fmt.Sprintf(`<svg id="%s" class="%s" style="%s"><use xlink:href="%s#%s" /></svg>`, x.Id, x.Class, x.Style, x.URL, x.Name)
}

// HTML returns the XML in a template.HTML
func (x XLink) HTML() template.HTML {
	return template.HTML(x.XML())
}

// Parse returns a sprite-map for a map of names->icons
func Parse(icons map[string]Icon) (string, error) {
	return parse(icons)
}

func parse(icons map[string]Icon) (string, error) {
	parent := &html.Node{
		Type:      html.ElementNode,
		DataAtom:  462339,
		Data:      "svg",
		Namespace: "svg",
		Attr: []html.Attribute{
			{
				Key: "style",
				Val: "display: none;",
			},
			{
				Key: "xmlns",
				Val: "http://www.w3.org/2000/svg",
			},
		},
	}

	for name, icon := range icons {
		page, err := html.Parse(strings.NewReader(icon.XML()))
		if err != nil {
			return "", err
		}
		node := page.FirstChild.LastChild.FirstChild
		node.Data = "symbol"

		for idx, attr := range node.Attr {
			if attr.Key == "id" {
				node.Attr[idx].Val = name
			} else if attr.Key == "width" || attr.Key == "height" {
				node.Attr[idx].Val = ""
			}
		}

		if parent.FirstChild == nil && parent.LastChild == nil {
			parent.FirstChild = node
			parent.LastChild = node
		} else {
			lc := parent.LastChild
			node.PrevSibling = lc
			parent.AppendChild(&html.Node{})
			lc.NextSibling = node
			parent.LastChild = node
		}
	}

	builder := &strings.Builder{}
	if err := html.Render(builder, parent); err != nil {
		return "", err
	}

	return builder.String(), nil
}
