# sprite
A library that takes in `Icon` interfaces and generates a sprite-map for them.

All [go-icon](https://gitea.com/go-icon) libraries support the required interface.

# Usage
```go
package main

import "fmt"

import "gitea.com/go-icon/octicon"
import "gitea.com/go-icon/sprite"

func main() {
    m := sprite.New()
    
    for _, name := range octicon.Icons {
        m.Add(name, octicon.Icon(name))
    }
    
    if err := m.Parse(); err != nil {
        panic(err)
    }
    
    fmt.Println(m.XML())

}
```

# License
[MIT License](LICENSE)